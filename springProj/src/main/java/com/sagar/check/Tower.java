package com.sagar.check;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
//sagarfvhvh
public class Tower 
{
	List<Floor> floorPlan = new ArrayList<Floor>();

	public void addFloor(int floorSize) {
		int previousFloorWindow = 0;
		if (ValidateWith(floorSize)) {
			previousFloorWindow = floorPlan.size() != 0 ? floorPlan.get(
					floorPlan.size() - 1).getWindow() : 0;
			if (previousFloorWindow != 0) {
				floorPlan.add(new Floor(floorSize, previousFloorWindow));
			} else {;
				floorPlan.add(new Floor(floorSize));
			}
		} else {
			throw new IllegalArgumentException(
					"Floors Higher Up Must Be The Same Or Smaller Than The Floor Below");
		}

		arrangeFloors(previousFloorWindow);
	}

	private boolean ValidateWith(int floorSize) {
		return floorPlan.size() > 0 ? floorPlan.get(floorPlan.size() - 1)
				.getWall() >= floorSize : true;
	}

	public String printFloor(int floorIndex) {
		return ((Floor) floorPlan.get(floorIndex)).getPattern().toString();
	}

	public void addFloorWithWindows(int floorSize, int desiredNumberOfWindows) {
		if (ValidateWith(floorSize)) {
			floorPlan.add(new Floor(floorSize, desiredNumberOfWindows));
		} else {
			throw new IllegalArgumentException(
					"Floors Higher Up Must Be The Same Or Smaller Than The Floor Below");
		}
		arrangeFloors(desiredNumberOfWindows);
	}

	void arrangeFloors(int noOfWindows) {
		int size = floorPlan.size();
		for (int i = 0; i < size - 1 && size > 1; i++) {
			compareFloor(floorPlan.get(i), floorPlan.get(i + 1), i);
		}
	}

	private void compareFloor(Floor floor1, Floor floor2,int posFirst) {
		int firstWallGroup=0;
		int secondWallGroup=0;
		int thirdWallGroup=0;
		int firstGroup=floor1.getFirstWindowGroup();
		int secondGroup=floor1.getSecondWindowGroup();
		int totalWall =0;
		if(floor1.getWindow()==floor2.getWindow()){
			totalWall = floor1.getWall()-(firstGroup+secondGroup);
			firstWallGroup = totalWall /3;
			secondWallGroup = firstWallGroup;
			thirdWallGroup = totalWall - (firstWallGroup+secondWallGroup);
			
			firstGroup =  firstGroup - 1;
			secondGroup =secondGroup + 1;
			StringBuffer pattern=new StringBuffer(Floor.createFloorWithSymbols(firstWallGroup, "X")+Floor.createFloorWithSymbols(firstGroup, "0")+Floor.createFloorWithSymbols(secondWallGroup, "X")+Floor.createFloorWithSymbols(secondGroup, "0")+Floor.createFloorWithSymbols(thirdWallGroup, "X"));
			Floor floor=new Floor();
			floor.setFirstWindowGroup(firstGroup);
			floor.setSecondWindowGroup(secondGroup);
			floor.setWall(floor1.getWall());
			floor.setWindow(floor1.getWindow());
			floor.setPattern(pattern);
			floorPlan.set(posFirst, floor);
		}
		
	}

}
