package com.sagar.check;

/**
Deepak Coment
/
class Floor {

	// wall size in floor
	int wall;
	// window size in floor
	int window;
	int firstWindowGroup;
	int secondWindowGroup;
	

	// pattren of the floor
	StringBuffer pattern;
	// symbol represents wall
	private final String WALL_SYMBOL = "X";
	// symbol represents window
	private final String WINDOW_SYMBOL = "0";

	// getters and setters ************************************
	public int getFirstWindowGroup() {
		return firstWindowGroup;
	}

	public void setFirstWindowGroup(int firstWindowGroup) {
		this.firstWindowGroup = firstWindowGroup;
	}

	public int getSecondWindowGroup() {
		return secondWindowGroup;
	}

	public void setSecondWindowGroup(int secondWindowGroup) {
		this.secondWindowGroup = secondWindowGroup;
	}
	public int getWall() {
		return wall;
	}

	public Floor() {
	
	}
	public void setWall(int wall) {
		this.wall = wall;
	}

	public int getWindow() {
		return window;
	}

	public void setWindow(int window) {
		this.window = window;
	}

	public StringBuffer getPattern() {
		return pattern;
	}

	public void setPattern(StringBuffer pattern) {
		this.pattern = pattern;
	}

	// ****************************************

	// simple floor without window
	public Floor(int wall) {
		this.wall = wall;
		pattern = printPattern(wall);
	}

	// simple floor with window
	public Floor(int wall, int window) {
		this.wall = wall;
		this.setFirstWindowGroup(window);
		this.window = window;
		pattern = printPattern(wall, window);
	}

	// print pattern of the floor without window
	StringBuffer printPattern(int wall) {
		StringBuffer floorPlan = new StringBuffer();
		for (int i = 0; i < wall; i++) {
			floorPlan.append(WALL_SYMBOL);
		}
		return floorPlan;
	}

	// print pattern of the floor with window
	StringBuffer printPattern(int wall, int window) {
		int initialwallPosition = 0;
		int lastWallPosition = 0;
		int wallWindowDifference = wall - window;
		if (wall == window || wall - window == 1) {
			initialwallPosition = 1;
			lastWallPosition = 1;
			window = wall - 2;
		} else {
			initialwallPosition = wallWindowDifference / 2;
			lastWallPosition = wallWindowDifference - initialwallPosition;
		}
		StringBuffer floorPlan = new StringBuffer(createFloorWithSymbols(
				initialwallPosition, WALL_SYMBOL)
				+ createFloorWithSymbols(window, WINDOW_SYMBOL)
				+ createFloorWithSymbols(lastWallPosition, WALL_SYMBOL));
		return floorPlan;
	}

	static String createFloorWithSymbols(int size, String symbol) {
		StringBuffer floorSymbol = new StringBuffer();
		for (int i = 0; i < size; i++) {
			floorSymbol.append(symbol);
		}
		return floorSymbol.toString();
	}

}